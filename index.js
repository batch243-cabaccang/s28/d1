// .pretty() to display out in a formatted way

// CREATE
// Instert an object into the database
// users is the collection name
db.users.insertOne({
  firstName: "Jane",
  lastName: "Doe",
  age: 21,
  contact: {
    phone: "1212121",
    email: "janedoe@gmail.com",
  },
  courses: ["CSS", "JavaScript", "Python"],
  department: "none",
});

// Instert multiple objects into the database
// users is the collection name
db.users.insertMany([
  {
    firstName: "Stephen",
    lastName: "Hawking",
    age: 76,
    contact: {
      phone: "87654321",
      email: "stephenhawking@gmail.com",
    },
    courses: ["Python", "React", "PHP"],
    department: "none",
  },
  {
    firstName: "Niel",
    lastName: "Armstrong",
    age: 82,
    contact: {
      phone: "87654321",
      email: "neilarmstrong@gmail.com",
    },
    courses: ["React", "Laravel", "Sass"],
    department: "none",
  },
]);

// READ

db.users.find();

// this is only for one parameter
// will return the object with the field firstName, and value of Stephen
db.users.find({ firstName: "Stephen" });

// this is fror multiple parameters
db.users.find({ lastName: "Armstrong", age: 82 }).pretty();

// UPDATE
// just to insert one to be updated
db.insertOne({
  firstName: "Test",
  lastName: "Test",
  age: 0,
  contact: {
    phone: "00000000",
    email: "test@gmail.com",
  },
  courses: [],
  department: "none",
});

db.users.updateOne(
  // Cirteria
  { firstName: "Bill" },
  //   fields to be update
  {
    $set: {
      lastName: "Gates",
      age: 65,
      contact: { phone: "123456789", email: "billgates@gmail.com" },
      courses: ["PHP", "Laravel", "HTML"],
      department: "Operations",
      status: "active",
    },
  }
);

db.users.updateMany(
  { department: "none" },
  {
    $set: {
      department: "HR",
    },
  }
);

// Replace One
db.users.replaceOne(
  { _id: ObjectId("637f100a83a97c621f474ea9") },
  {
    firstName: "Chris",
    lastName: "Mortel",
    age: 14,
    contact: { phone: "123456789", email: "chris@gmail.com" },
    courses: ["PHP", "Laravel", "HTML"],
    department: "Operations",
  }
);

// DELETE
db.users.insertOne({
  firstName: "Test",
  lastName: "Test",
  age: 0,
  contact: {
    phone: "00000000",
    email: "test@gmail.com",
  },
  courses: [],
  department: "none",
});

db.users.deleteOne({ firstName: "Test" });

// Do not use db.collectionName.deleteMany();
// always make sure to always have a criteria

// ADDVANCE QUERIES
// embeded onjects
db.users.find({ contact: { phone: "1212121", email: "janedoe@gmail.com" } });
// to find documents with embeded objects and access specific field
db.users.find({ "contact.phone": "1212121" });

// querying an array with exact order of elements
db.users.find({ courses: ["Python", "React", "PHP"] });

// querying an array with any order of elements .has() or .cludes() kung baga.
db.users.find({ courses: { $all: ["React"] } });
